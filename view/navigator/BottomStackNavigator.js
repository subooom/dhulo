import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'

import MapScreen from 'view/screens/MapView'
import ChuckyScreen from 'view/screens/Chucky'
import SettingsScreen from 'view/screens/Settings'

export default createTabNav({
  Map: { screen: MapScreen },
  Credits: { screen: ChuckyScreen },
  Settings: { screen: SettingsScreen },
}, {
  initialRouteName: 'Map',
  activeColor: '#f0edf6',
  inactiveColor: '#3e2465',
  barStyle: { backgroundColor: '#694fad' },
});