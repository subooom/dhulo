import React, { Component } from 'react';
import { Slider, Dimensions, View } from 'react-native';
import { Form, Title, Toast, Item, Label, Input, Icon, Button, Text } from 'native-base';

import PhoneStorage from "app/core/PhoneStorage";
import Firebase from "app/core/Firebase";

const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

const sliderLabels = ['', 'Light', 'Medium', 'Heavy', 'Dense', 'Very Dense']
const sliderColors = ['', '#008809', '#7fcc00', '#ffcf00', '#ff7300', '#ff0000']

const phoneStorage = new PhoneStorage()
const firebase = new Firebase()

export default class PendingForm extends Component {

  constructor(props) {
    super(props);
    props.height = height
    this.state = {
      slider: 'Heavy',
      sliderColor: '#ffcf00',
      email: '',
      latitude: props.latitude,
      longitude: props.longitude,
      level: props.level,
      radius: props.radius,
      credit: '0',
      isHidden: global.userEmail ? true : false
    }
  }
  getColor(level){
    console.log(level)
  }
  async storeToDatabase(email, latitude, longitude, radius, level, credit, timestamp){
    const attributes = {email, latitude, longitude, radius, level, credit, timestamp}
    global.userEmail = await phoneStorage.get('email').then(email => {
      if(email !== null) {
        this.setState({
          isHidden: true,
          email
        })
        return email
      }
    })
    if(this.state.isHidden){
      email = global.userEmail
    }
    if(!global.userEmail){
      global.userEmail = email
      phoneStorage.set('email', email)
    }
    firebase.pushToDatabase('Dhulo',attributes).then( _ => {
      //success callback
      Toast.show({
        text: 'Thank you for contributing! :)',
        duration: 3000,
        position: "top",
        type: 'success'
      })

      this.setState({
        email: '',
        isHidden: true,
        level: 25,
        radius: 150,
        slider: sliderLabels[3],
        sliderColor: sliderColors[3]
      }, () => this.props.onSubmit(attributes))
    }).catch( error =>{
      //error callback
      console.log('error ' , error)
      Toast.show({
        text: 'Oops, something bad happened! Please try again.',
        duration: 3000,
        position: "top",
        type: 'danger'
      })
    })
  }
  renderEmailInput(){

    if(global.userEmail === undefined && this.state.isHidden){
      return(
        <Item floatingLabel style={{paddingVertical: 10}}>
          <Label style={{top: 10}}>Email (you need to enter just once)</Label>
          <Input onChangeText={(email) => this.setState({email})}
            value={this.state.email}
          />
          <Icon active name='mail' />
        </Item>
      )
    }
  }

  render() {
    var email = this.state.email
    var latitude = this.state.latitude
    var longitude = this.state.longitude
    var radius = this.state.radius
    var level = this.state.level
    var timestamp = Math.floor(Date.now() / 1000)

    return (
      <View style={{zIndex:20, paddingTop: 22, backgroundColor: '#eeeeee'}}>
        <Form style={{backgroundColor: 'white'}}>
          <Title style={{color: 'black', fontFamily: 'Raleway', fontSize: 20, zIndex:20, letterSpacing: 3}}>Give your rating for </Title>
          <Title style={{color: 'black', fontFamily: 'Raleway', fontSize: 20, zIndex:20, letterSpacing: 3}}>{this.state.latitude.toFixed(4)} N, {this.state.longitude.toFixed(4)} E</Title>
          {this.renderEmailInput()}
          <Item style={{height: 70, paddingVertical: 0}}>
            <Slider
              style={{ width: width-50 }}
              step={1}
              minimumValue={20}
              maximumValue={300}
              value={this.state.radius}
              onValueChange={radius => {
                  this.setState({radius})
                  return this.props.onRadiusChange(radius)
                }
              }
              onSlidingComplete={this.props.onRadiusChange}
              >
            </Slider>
            <Label style={{position: 'absolute', top: 10, color: this.state.sliderColors}}>Radius - {Math.floor((this.state.radius/300)*100)}%</Label>
          </Item>
          <Item style={{height: 70, paddingVertical: 0}}>
            <Slider
              style={{ width: width-50 }}
              step={1}
              minimumValue={1}
              maximumValue={50}
              minimumTrackTintColor={sliderColors[Math.ceil(this.state.level/10)]}
              maximumTrackTintColor={sliderColors[Math.ceil(this.state.level/10)]}
              thumbTintColor={sliderColors[Math.ceil(this.state.level/10)]}
              value={this.state.level}
              onValueChange={level => {
                  this.setState({level})
                  return this.props.onLevelChange(sliderColors, level)
                }
              }
              >
            </Slider>
            <Label style={{position: 'absolute', top: 10, color: sliderColors[Math.ceil(this.state.level/10)]}}>Level - {sliderLabels[Math.ceil(this.state.level/10)]}</Label>
          </Item>
        </Form>
        <Button success onPress={ () =>this.storeToDatabase(email, latitude, longitude, radius, level, 0, timestamp) } style={{marginTop: 24, marginLeft: 24, marginBottom: 24}}><Text> Submit</Text><Icon name="send"></Icon></Button>
        {/* <Button warning onPress={ () => {
          phoneStorage.flush()
          global.email = null
        } } style={{marginTop: 24, marginLeft: 24}}><Text> Reset Storage</Text><Icon name="trash"></Icon></Button> */}
      </View>
    );
  }
}