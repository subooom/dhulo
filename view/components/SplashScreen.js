import React, { Component } from "react"
import {StyleSheet, StatusBar, Image, Dimensions, ActivityIndicator } from "react-native"

import {Container, Text} from "native-base"

import Images from '@assets/images'

const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

export default class SplashScreen extends Component {

  constructor() {
    super();
    this.state = {
      marginLeft: 20,
      logoTopPosition: 15,
      sloganTopPosition: 150,
      sloganSubTopPosition: 180,
    }
    this._hideStatusBar()
  }

  _hideStatusBar = () => {
    StatusBar.setHidden(true, 'slide');
  }

  render() {
    return (
      <Container styles={styles.container} >
          <Image
            style={styles.bgImg}
            source={Images.bgImg}
          >
          </Image>
          <Image
            style={{
              position: 'absolute',
              height: 150,
              width,
              top: this.state.logoTopPosition,
              zIndex: 20
            }}
            source={Images.logo}
          >
          </Image>
          <Text style={{
            position: 'absolute',
            top: this.state.sloganTopPosition,
            left: 20,
            color: '#F9F4F5',
            fontSize: 18,
            letterSpacing: 3,
            zIndex: 20
          }}>
            DHULO Udiracha? Not sure?
          </Text>
          <Text style={{
            position: 'absolute',
            top: this.state.sloganSubTopPosition,
            left: 20,
            color: 'white',
            fontSize: 25,
            letterSpacing: 3
          }}>
            FIND OUT!
          </Text>
          <ActivityIndicator size={30} style={{
            position: 'absolute',
            top: this.state.sloganSubTopPosition+50,
            left: 20,}} color={'white'}></ActivityIndicator>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    flex: 1,
    width: null,
    height: null
  },
  bgImg:{
    height,
    width,
    resizeMode: "cover",
  },
  content:{
    position: 'absolute',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    zIndex:20
  },
})