import React, { Component } from 'react'

import {StyleSheet} from 'react-native'

import {Button, Icon, Text} from 'native-base'

const styles = StyleSheet.create({
  contributeButton:{
    zIndex: 1,
    position: 'absolute',
    bottom: 10,
    right: 10
  }
});
export default class ContributeButton extends Component{
  constructor(props) {
    super(props);
    this.props = props
    console.log(this.props)
    this.state = {
      contributeMode: false
    }
  }

  toggleContributeMode = () => {
    const oldState = this.state.contributeMode
    this.setState({contributeMode:!oldState}, () => this.props.onToggle(this.state.contributeMode))
  }

  render(){
    return(
    <Button rounded style={styles.contributeButton} onPress={this.toggleContributeMode.bind(this)} iconLeft light>
      <Icon name= {this.state.contributeMode ? 'pulse':'hand'} />
      <Text> {this.state.contributeMode ? 'Done':'Contribute'}</Text>
    </Button>)
  }
}