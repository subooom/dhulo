import MapView from 'react-native-maps';
import React, {Component} from 'react'
import { StyleSheet, Image, View, StatusBar, Dimensions } from 'react-native'
import { Container, Text } from "native-base"

import Firebase from "app/core/Firebase";

const firebase = new Firebase();

import Themes from 'app/utils/Themes'
import Helpers from 'app/helpers/Helpers'

import Images from '@assets/images'

const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

const sliderColors = ['', '#008809', '#7fcc00', '#ffcf00', '#ff7300', '#ff0000']

import SplashScreen from 'view/components/SplashScreen'

import ContributeForm from 'view/components/ContributeForm';
import PendingForm from 'view/components/PendingForm';

import ContributeButton from 'view/components/ContributeButton'

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height,
    width,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  logo:{
    position: 'absolute',
    height: 100,
    width: 250,
    top: 15,
    left: 50,
    zIndex: 1,
  },
  map: {
    // transform: [
    //   { perspective: 600 },
    //   { rotateX: '45deg' },
    //   {translateX: -150},
    //   {translateY: 130},
    // ],
    // width: width+200
  },
  container:{
   flex: 1,
  }
});

export default class MapScreen extends Component {
  constructor(props) {
    super(props);
    this.props = props
    this.maker = null
    this.state = {
      active: false,
      latitude: 27.7172,
      longitude: 85.3240,
      myLatitude: 27.7172,
      myLongitude: 85.3240,
      clickedLatLon: [],
      mapTheme: Themes.BROWNTHEME,
      loading: true,
      contributeMode: false,
      showPendingForm: false,
      showForm: false,
      circleColor: Helpers.hexToRGB('#ffcf00', 0.3),
      colors: [],
      level: 25,
      data: [],
      radius: 150,
      formHeight: 0
    }
  }

  async componentWillMount(){
    await this.getData()
  }

  async getData() {
    this.setState({loading: true})
    await firebase.getFromDatabase('Dhulo', 10).then( snapshot => {
      const data = []
      snapshot.forEach(item =>{
        data.push({
          key: item.key,
          email: item.val().email,
          latitude: item.val().latitude,
          longitude: item.val().longitude,
          radius: item.val().radius,
          level: item.val().level,
          credit: item.val().credit,
          timestamp: item.val().timestamp
        })
      })
      this.setState({
        loading: false,
        data
      }, _ => this.props.onDataLoaded(this.state.loading))
    }).catch( error =>{
      //error callback
      console.error(error)
    })
  }

  handleMapClick = (event) => {
    if(this.state.contributeMode){
        const hiddenState = this.state.showForm
        const coordinate = event.nativeEvent.coordinate

        const data = [{"latitude": coordinate.latitude, "longitude": coordinate.longitude}]
        this.setState({
          clickedLatLon: data,
          showForm: !hiddenState,
          latitude: data[0].latitude,
          longitude: data[0].longitude,
        })
    }
  }

  renderPendingForm = () =>{
    return(
      <PendingForm
        radius={this.state.radius}
        level={this.state.level}
        onHeightDetermined={height => this.setState({formHeight: height})}
        onRadiusChange={(radius) => this.setState({radius})}
        onLevelChange={(colors, level) => this.setState({
            level,
            colors,
            circleColor: Helpers.hexToRGB(colors[Math.ceil(level/10)], 0.6)
          })
        }
        latitude={this.state.latitude}
        longitude={this.state.longitude}
        onSubmit={ newData => {
          var joined = this.state.data.concat(newData);
          this.setState({ data: joined, showForm:false })
        }}
        style={{
          zIndex: 5,
          position: 'absolute',
          top: 10,
          left: 10,
        }}
      />
    )
  }

  createDhuloZone = (dhulo, type) => {
    switch(type){
      case 'pending':
        console.log('pending')
        return(
          <View>
            <MapView.Marker
              draggable
              tooltip={true}
              coordinate={{
                latitude: dhulo.latitude,
                longitude: dhulo.longitude,
              }}
              image={Images.nepaliLogoMini}
              onCalloutPress={() => {
                var pendingFormIsVisible = this.state.pendingFormIsVisible
                this.setState({showPendingForm: !pendingFormIsVisible
                })
              }}
            >
              <MapView.Callout>
                  <Text>Click</Text>
              </MapView.Callout>
            </MapView.Marker>
            <MapView.Circle
              center={{
                        latitude: dhulo.latitude,
                        longitude: dhulo.longitude
                      }}
              radius= { dhulo.radius }
              strokeWidth = { 10 }
              strokeColor = { Helpers.hexToRGB(sliderColors[Math.ceil(dhulo.level/10)], 0.6) }
              fillColor = { Helpers.hexToRGB('#000000', 0.6) }
            >
            </MapView.Circle>
          </View>
        );
      case 'approved':
        console.log('approved')
        return(
          <View>
            <MapView.Marker
              draggable
              tooltip={true}
              coordinate={{
                latitude: dhulo.latitude,
                longitude: dhulo.longitude,
              }}
              image={Images.nepaliLogoMini}
            >
              <MapView.Callout>
                <Text>djaklsflaskdfl</Text>
              </MapView.Callout>
            </MapView.Marker>
            <MapView.Circle
              center={{
                        latitude: dhulo.latitude,
                        longitude: dhulo.longitude
                      }}
              radius= { dhulo.radius }
              strokeWidth = { 1 }
              strokeColor = { Helpers.hexToRGB(sliderColors[Math.ceil(dhulo.level/10)], 0.6)  }
              fillColor = { Helpers.hexToRGB(sliderColors[Math.ceil(dhulo.level/10)], 0.6)  }
            >
            </MapView.Circle>
          </View>
        );
      case 'new':
        console.log('new')

        return(
          <View>
            <MapView.Marker
              draggable
              tooltip={true}
              coordinate={{
                latitude: dhulo.latitude,
                longitude: dhulo.longitude,
              }}
              image={Images.nepaliLogoMini}
            >
              <MapView.Callout>
                <Text>djaklsflaskdfl</Text>
              </MapView.Callout>
            </MapView.Marker>
            <MapView.Circle
              center={{
                        latitude: dhulo.latitude,
                        longitude: dhulo.longitude
                      }}
              radius= { this.state.radius }
              strokeWidth = { 1 }
              strokeColor = { this.state.circleColor }
              fillColor = { this.state.circleColor }
            >
            </MapView.Circle>
          </View>
        );
    }

  }

  handleContributeModeToggle = (status) => {
    const oldState = this.state.contributeMode
    if(status){
      this.setState({ mapTheme: Themes.DARK_GOOGLE, contributeMode: !oldState}, () => this.map._updateStyle())
    }else{
      this.setState({ mapTheme: Themes.BROWNTHEME, contributeMode: !oldState, showForm: false }, () => this.map._updateStyle())
    }
  }

  render() {
    navigator.geolocation.getCurrentPosition(location =>{
      const { latitude, longitude } = location.coords; // destructuring assignment
      this.setState({
        myLatitude:latitude, // object property shorthand
        myLongitude:longitude
      });
    })
    if(this.state.loading){
      return(
        <SplashScreen>
        </SplashScreen>
      )
    }
    else{
      if(this.marker){ this.marker.showCallout() }
      return (
        <Container style ={styles.container}>
          {/* {this.state.showForm || !this.state.contributeMode && !this.state.pendingMode ? <View></View>
          : <Body style={{ backgroundColor: 'transparent', position: 'absolute', zIndex: 21, top: 80}}>
              <Segment style={{ backgroundColor: 'transparent'}}>
                <Button first onPress={_ => this.setState({pendingMode: false, contributeMode: true})} active={this.state.contributeMode}><Text>Contribute Mode</Text></Button>
                <Button last onPress={_ => this.setState({pendingMode: true, contributeMode: false})} active={this.state.pendingMode} ><Text>Pending Data</Text></Button>
              </Segment>
            </Body>
          } */}

          {!this.state.contributeMode ?
          <Image
            style={styles.logo}
            source={Images.logoWhite}
          >
          </Image>
          : <Text style={styles.logo}>Please tap on the area that you know is polluted.</Text>
          }

          {this.state.contributeMode ?
          <Image
            style={{zIndex: 1, position: 'absolute', bottom: 10, left: 10}}
            source={Images.legend}
          >
          </Image>
          : <View></View>
          }
          <ContributeButton onToggle = {status => this.handleContributeModeToggle(status)}></ContributeButton>
          {
            this.state.showPendingForm ?
            this.renderPendingForm() : <View></View>
          }
          {
            this.state.showForm ?
              <ContributeForm
              radius={this.state.radius}
              level={this.state.level}
              onHeightDetermined={height => this.setState({formHeight: height})}
              onRadiusChange={(radius) => this.setState({radius})}
              onLevelChange={(colors, level) => this.setState({
                  level,
                  colors,
                  circleColor: Helpers.hexToRGB(colors[Math.ceil(level/10)], 0.6)
                })
              }
              latitude={this.state.latitude}
              longitude={this.state.longitude}
              onSubmit={ newData => {
                var joined = this.state.data.concat(newData);
                this.setState({ data: joined, showForm:false })
              }}
              style={{
                zIndex: 5,
                position: 'absolute',
                top: 10,
                left: 10,
                flex:1
              }}/>
            : <View></View>
          }
          <MapView
            onPress={ this.handleMapClick }
            style={{
              position: 'absolute',
              right: 0,
              bottom: 0,
              left: 0,
              top: 0,
              alignItems: 'center',
            }}
            ref={map => { this.map = map }}
            region={{
              latitude: this.state.latitude,
              longitude: this.state.longitude,
              latitudeDelta: 0.0315,
              longitudeDelta: 0.0121,
            }}
            provider = { MapView.PROVIDER_GOOGLE }
            customMapStyle = { this.state.mapTheme }
          >
            <MapView.Marker
                draggable
                tooltip={true}
                coordinate={{
                  latitude: this.state.myLatitude,
                  longitude: this.state.myLongitude,
                }}
                image={Images.nepaliLogoMini}
                onCalloutPress={() => {
                  var pendingFormIsVisible = this.state.pendingFormIsVisible
                  this.setState({showPendingForm: !pendingFormIsVisible
                  })
                }}
              ></MapView.Marker>
            {this.state.data.map(dhulo =>{
                if(this.state.contributeMode){

                  return(
                    <View key={dhulo.key}>
                      {dhulo.credit < 5 ? this.createDhuloZone(dhulo, 'pending') : this.createDhuloZone(dhulo, 'approved')}
                    </View>
                  )
                }
                else if(!this.state.contributeMode && dhulo.credit>=5){
                  return(
                    <View key={dhulo.key}>
                      {this.createDhuloZone(dhulo, 'approved')}
                    </View>
                  )
                }
              })
            }
            {this.state.showForm ?
              this.state.clickedLatLon.map(dhulo =><View key={dhulo.latitude}>{this.createDhuloZone(dhulo, 'new')}</View>)  : null}
          </MapView>
        </Container>
      );
    }
  }
}
