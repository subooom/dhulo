import React, { Component } from 'react';
import { Container, Header, Title,  Content, Button, ListItem, Separator, Text, Icon, Left, Body, Right, Switch } from 'native-base';
import {StatusBar} from 'react-native'
import { createStackNavigator } from 'react-navigation';
import MapThemeScreen from './settings/MapTheme'
import UserScreen from './settings/User'

const SettingsNavigator = createStackNavigator({
  MapTheme: { screen: MapThemeScreen },
  User: { screen: UserScreen },
});
import PhoneStorage from "../services/PhoneStorage";
const phoneStorage = new PhoneStorage()

export default class SettingsScreen extends Component {
  constructor(){
    super()
    this.state = {
      email: null
    }
    console.log(SettingsNavigator.MapTheme)

    this._hideStatusBar()
  }
  static navigationOptions = {

    title: 'Settings',
    drawerIcon:(
      <Icon name="hand"/>
    )
  }
  _hideStatusBar = () => {
    StatusBar.setHidden(true, 'slide');
  }
  componentWillMount(){
    this.getUserData();
  }
  async getUserData(){
    await phoneStorage.get('email').then(email => {
      if(email !== null){
        console.log(email)
        this.setState({
          email
        })
      }
    })
  }
  render() {
    return (
      <Container>
        <Header style={{backgroundColor: '#000000'}}>
          <Left>
            <Icon name='arrow-back' onPress={() => this.goBack()} style={{color: 'white'}} />
          </Left>
          <Body>
            <Title>Settings</Title>
          </Body>
          <Right>
            <Icon onPress={() => this.props.navigation.openDrawer()} style={{color: 'white'}}  name='menu' ></Icon>
          </Right>
        </Header>
        <Content>
          <Separator bordered>
            <Text>GENERAL</Text>
          </Separator>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#FF9501" }}>
                <Icon active name="map" />
              </Button>
            </Left>
            <Body>
              <Text>Map Theme</Text>
            </Body>
            <Right>
              <Text>Brown Theme</Text>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="person" />
              </Button>
            </Left>
            <Body>
              <Text>User</Text>
            </Body>
            <Right>
              <Text>{this.state.email === null ? 'No account yet.' : this.state.email}</Text>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="bluetooth" />
              </Button>
            </Left>
            <Body>
              <Text>Bluetooth</Text>
            </Body>
            <Right>
              <Text>On</Text>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}