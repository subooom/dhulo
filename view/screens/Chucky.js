import React, { Component } from "react"
import {
    StyleSheet,
    Text,
    Image,
    View,
    Dimensions,
    LayoutAnimation
} from "react-native"

import {
  Icon,
  Button,
  Container,
  Content,
  Fab,
} from "native-base"
import Images from '@assets/images'

const width = Dimensions.get("window").width

const chucky = require("chucknorris-quotes");

export default class ChuckyScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      joke: '',
    };
    this.state = { marginLeft: 20 };
    this.getJoke();
  }

  getJoke(){
    LayoutAnimation.spring();
    this.setState({
      marginLeft: this.state.paddingLeft + 500
    })
    chucky.getRandomJoke().then((response) => {

      this.setState({
        marginLeft: 20
      })
      this.setState(_=>{
        return {
          joke: response.data.value.joke.replace('&quot;', '\'')
        }
      })

    });
  }

  static navigationOptions = {
    drawerIcon:(
      <Icon name="ios-flame"/>
    ),
  }

  render() {
    return (
      <Container styles={styles.container}>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: '', position: 'absolute', zIndex: 1}}
          position="topRight"
          onPress={() => this.props.navigation.openDrawer()}>
          <Icon name="menu" style={{color:'#231651'}} />
        </Fab>
        <Image
          style={styles.logo}
          source={Images.logo}
        ></Image>
        <Content styles={styles.content} contentContainerStyle={marginLeft=this.state.marginLeft}>
            <Text style={styles.thankxText}>TO THE ARTISTS AND DEVELOPERS THAT MADE THIS POSSIBLE</Text>
            <Image
              source={Images.thankx}
            ></Image>
            <Text style={styles.joke}>{this.state.joke}</Text>
            <View>
              <Text style={styles.title}>Fonts Used</Text>
              <Text style={styles.subtitle}>1. Ananda Sumitra - Ananda K. Maharjan</Text>
              <Image
                source={Images.anandaSumitra}
                style={{width, height: 350}}
              ></Image>
              <Text style={styles.subtitle}>2. Raleway - Google Fonts</Text>
              <Text style={{fontFamily: 'raleway', padding: 25, letterSpacing: 2, fontSize: 45, fontWeight: '100'}}>The spectacle before us was indeed sublime.</Text>
            </View>
        </Content>
      </Container>

    );
  }
}
const styles = StyleSheet.create({
  container:{
    backgroundColor: '#FBF5F3'
  },
  logo:{
    height: 120,
    width: 250,
    top: 15,
  },
  thankxText:{
    fontSize: 30,
    fontFamily: 'Raleway',
    letterSpacing: 3,
    padding: 10
  },
  title:{
    padding: 20,
    fontFamily: 'Raleway',
    letterSpacing: 3,
    fontWeight: '400',
    fontSize: 30
  },
  subtitle:{
    padding: 25,
    fontFamily: 'Raleway',
    letterSpacing: 3,
    fontWeight: '400',
    fontSize: 20
  },
  joke: {
    fontSize: 15,
    padding: 20,
    color: '#000000',
    letterSpacing: 2,
    fontWeight: "600"
  },
  content:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
})