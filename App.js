import React, {Component} from 'react';
import {StyleSheet, Text} from 'react-native';

import Helpers from 'app/helpers/Helpers'

import MapScreen from 'view/screens/MapView'


export default class App extends Component{
  constructor(props){
    super(props)
    this.state = {
      loading: true
    }
    console.ignoredYellowBox = [
      'Setting a timer'
    ];
    Helpers.hideStatusBar()
  }
  render() {
    return (
      <MapScreen onDataLoaded={(loading)=> this.setState({loading})}></MapScreen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
