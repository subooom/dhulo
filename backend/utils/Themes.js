export default Themes = {
  "PARTIAMO": [
    {
      "featureType": "all",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#fff2d4"
      }]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#385427"
        },
        {
          "weight": "0.30"
        },
        {
          "saturation": "-75"
        },
        {
          "lightness": "5"
        },
        {
          "gamma": "1"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "labels.text.fill",
      "stylers": [{
          "color": "#385427"
        },
        {
          "saturation": "-75"
        },
        {
          "lightness": "5"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "labels.text.stroke",
      "stylers": [{
          "color": "#fff2d4"
        },
        {
          "visibility": "on"
        },
        {
          "weight": "6"
        },
        {
          "saturation": "-28"
        },
        {
          "lightness": "0"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "labels.icon",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#385427"
        },
        {
          "weight": "1"
        }
      ]
    },
    {
      "featureType": "landscape",
      "elementType": "all",
      "stylers": [{
          "color": "#fff2d4"
        },
        {
          "saturation": "-28"
        },
        {
          "lightness": "0"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "road",
      "elementType": "all",
      "stylers": [{
          "color": "#385427"
        },
        {
          "visibility": "simplified"
        },
        {
          "saturation": "-75"
        },
        {
          "lightness": "5"
        },
        {
          "gamma": "1"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#385427"
        },
        {
          "weight": 8
        },
        {
          "saturation": "-28"
        },
        {
          "lightness": "0"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#385427"
        },
        {
          "weight": 8
        },
        {
          "lightness": "5"
        },
        {
          "gamma": "1"
        },
        {
          "saturation": "-75"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [{
          "visibility": "simplified"
        },
        {
          "color": "#385427"
        },
        {
          "saturation": "-75"
        },
        {
          "lightness": "5"
        },
        {
          "gamma": "1"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#385427"
        },
        {
          "saturation": "-75"
        },
        {
          "lightness": "5"
        },
        {
          "gamma": "1"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text",
      "stylers": [{
          "visibility": "simplified"
        },
        {
          "color": "#fff2d4"
        },
        {
          "saturation": "-28"
        },
        {
          "lightness": "0"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    }
  ],
  "BROWNTHEME": [
    {
      "featureType": "all",
      "elementType": "labels",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#ffffff"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "labels",
      "stylers": [{
        "visibility": "simplified"
      }]
    },
    {
      "featureType": "landscape",
      "elementType": "all",
      "stylers": [{
          "color": "#f9ddc5"
        },
        {
          "lightness": -7
        }
      ]
    },
    {
      "featureType": "landscape",
      "elementType": "labels",
      "stylers": [{
        "visibility": "on"
      }]
    },
    {
      "featureType": "poi",
      "elementType": "labels",
      "stylers": [{
          "visibility": "off"
        },
        {
          "weight": "0.01"
        },
        {
          "invert_lightness": true
        },
        {
          "gamma": "0.78"
        }
      ]
    },
    {
      "featureType": "poi.business",
      "elementType": "all",
      "stylers": [{
          "color": "#645c20"
        },
        {
          "lightness": 38
        }
      ]
    },
    {
      "featureType": "poi.government",
      "elementType": "all",
      "stylers": [{
          "color": "#9e5916"
        },
        {
          "lightness": 46
        }
      ]
    },
    {
      "featureType": "poi.government",
      "elementType": "labels",
      "stylers": [{
        "visibility": "on"
      }]
    },
    {
      "featureType": "poi.medical",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#813033"
        },
        {
          "lightness": 38
        },
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "all",
      "stylers": [{
          "color": "#645c20"
        },
        {
          "lightness": 39
        }
      ]
    },
    {
      "featureType": "poi.school",
      "elementType": "all",
      "stylers": [{
          "color": "#a95521"
        },
        {
          "lightness": 35
        }
      ]
    },
    {
      "featureType": "poi.sports_complex",
      "elementType": "all",
      "stylers": [{
          "color": "#9e5916"
        },
        {
          "lightness": 32
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "all",
      "stylers": [{
          "color": "#813033"
        },
        {
          "lightness": 43
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "road.local",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#f19f53"
        },
        {
          "weight": 1.3
        },
        {
          "visibility": "off"
        },
        {
          "lightness": 16
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "geometry.stroke",
      "stylers": [{
          "color": "#f19f53"
        },
        {
          "lightness": -10
        },
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [{
        "lightness": 38
      }]
    },
    {
      "featureType": "transit",
      "elementType": "labels",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "transit.line",
      "elementType": "all",
      "stylers": [{
          "color": "#813033"
        },
        {
          "lightness": 22
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "all",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "water",
      "elementType": "all",
      "stylers": [{
          "color": "#0fb3ed"
        },
        {
          "saturation": -69
        },
        {
          "gamma": 0.99
        },
        {
          "lightness": 43
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels",
      "stylers": [{
        "visibility": "on"
      }]
    }
  ],
  "SANANDREAS": [
    {
      "featureType": "all",
      "elementType": "labels",
      "stylers": [{
        "visibility": "on",
      }]
    },
    {
      "featureType": "all",
      "elementType": "labels.text",
      "stylers": [{
        "visibility": "simplified"
      }]
    },
    {
      "featureType": "all",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "landscape",
      "elementType": "geometry",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#38692d"
        }
      ]
    },
    {
      "featureType": "landscape.man_made",
      "elementType": "geometry",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#eeff"
        }
      ]
    },
    {
      "featureType": "landscape.man_made",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#000000"
      }]
    },
    {
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#386c28"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry.fill",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#ffffff"
        }
      ]
    },
    {
      "featureType": "poi.attraction",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#ffffff"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "poi.business",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#ffffff"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "poi.government",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#ffffff"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "poi.medical",
      "elementType": "geometry",
      "stylers": [{
          "color": "#fcfcfc"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "poi.medical",
      "elementType": "labels",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#788c40"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "poi.place_of_worship",
      "elementType": "geometry",
      "stylers": [{
          "invert_lightness": true
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "poi.school",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#ffffff"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "poi.sports_complex",
      "elementType": "geometry",
      "stylers": [{
        "color": "#ffffff"
      }]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [{
        "color": "#000000"
      }]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [{
        "color": "#000000"
      }]
    },
    {
      "featureType": "road.local",
      "elementType": "geometry",
      "stylers": [{
        "visibility": "off",
        "color": "#000000"
      }]
    },
    {
      "featureType": "transit",
      "elementType": "geometry.fill",
      "stylers": [{
          "weight": "0.01"
        },
        {
          "saturation": "-33"
        },
        {
          "visibility": "on"
        },
        {
          "hue": "#ff0000"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [{
          "color": "#000000"
        },
        {
          "weight": "0.01"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry.fill",
      "stylers": [{
          "visibility": "on"
        },
        {
          "color": "#ff0000"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#00bbee"
      }]
    }
  ],
  'DARK_GOOGLE': [
    {
      "elementType": "geometry",
      "stylers": [{
        "color": "#212121"
      }]
    },
    {
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#212121"
      }]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [{
        "color": "#757575"
      }]
    },
    {
      "featureType": "administrative.country",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#9e9e9e"
      }]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "administrative.locality",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#bdbdbd"
      }]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [{
        "color": "#181818"
      }]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#1b1b1b"
      }]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#2c2c2c"
      }]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#8a8a8a"
      }]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [{
        "color": "#373737"
      }]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [{
        "color": "#3c3c3c"
      }]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [{
        "color": "#4e4e4e"
      }]
    },
    {
      "featureType": "road.local",
      "elementType": "labels",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#616161"
      }]
    },
    {
      "featureType": "transit",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#757575"
      }]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "color": "#000000"
      }]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#3d3d3d"
      }]
    }
  ]
}