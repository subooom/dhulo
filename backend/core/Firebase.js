const firebase = require("firebase");

class Firebase {
  constructor() {
    this.firebase = firebase;
    this.config = {
      databaseURL: "https://crowdsourced-review-of-p-b0e13.firebaseio.com",
      projectId: "crowdsourced-review-of-p-b0e13",
    };

    !this.firebase.apps.length && this.firebase.initializeApp(this.config);
  }

  async getFromDatabase(databaseName, paginationValue = 10){

    return await this.firebase
      .database()
      .ref(databaseName + '/')
      .once('value', snapshot => snapshot)
      .catch((error) => {
        console.log('error ', error)
        return false
      });
  }

  async pushToDatabase(databaseName, attributes) {
    console.log(databaseName, attributes)

    return await this.firebase
      .database()
      .ref(databaseName + '/')
      .push(attributes)
  }
}

export default Firebase;