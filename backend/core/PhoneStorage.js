import {AsyncStorage} from 'react-native'

class PhoneStorage {
  constructor(){
    var timestamp = Math.floor(Date.now() / 1000)

    console.log('Linked to AsyncStorage at '+timestamp+'.')
  }
  async set(key, value) {
    try {
      await AsyncStorage.setItem('Dhulo@user:'+key, value)
    } catch (error) {
      console.log(error)
    }
  }
  async get(label){
    try {
      return await AsyncStorage.getItem('Dhulo@user:'+label);
    } catch (error) {
      return error
    }
  }
  async flush(){
    await AsyncStorage.clear(_ => console.log('Storage Flushed.'))
  }
}
export default PhoneStorage;