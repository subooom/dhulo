const Images = {
  bgImg: require("./img/bg_1.jpg"),
  drawerBg: require("./img/drawer_bg.jpg"),
  screen: require("./img/screen.png"),
  logo: require("./img/logo.png"),
  logoWhite: require("./img/logo-white.png"),
  nepaliLogo: require("./img/nepalilogo.png"),
  nepaliLogoMini: require("./img/nepalilogomini.png"),
  legend: require("./img/legend.png"),
  logoGantabya: require("./img/logo-gantabya.png"),
  giphy: require("./img/giphy.gif"),
  thankx: require("./img/thankx.gif"),
  anandaSumitra: require("./img/ananda_sumitra.jpg"),
  me: require("./img/me.png"),
};

export default Images;