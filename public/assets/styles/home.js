import {StyleSheet, Dimensions} from 'react-native'

const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

export default StyleSheet.create({
  container:{
    flex: 1,
  },
  content:{
    position: 'absolute',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  bgImg:{
    height,
    width,
    resizeMode: "cover"
  },
  slogan:{
    position: 'absolute',
    top: 150,
    left: 20,
    color: 'white',
    fontSize: 18,
    fontFamily: 'Raleway',
    letterSpacing: 3
  },
  slogan_title:{
    position: 'absolute',
    top: 180,
    left: 20,
    color: 'white',
    fontSize: 25,
    fontFamily: 'Raleway Bold',
    letterSpacing: 3
  },
  search:{
    position: 'absolute',
    bottom: 150,
    left: 20,
    right: 20,
    color: 'red',
    fontSize: 18,
    fontFamily: 'Raleway',
    letterSpacing: 5,
    overflow: 'visible'
  },
  search_label:{
    color: '#fff',
    fontFamily: 'Raleway Light',
    letterSpacing: 5,
  },
  search_input:{
    color: '#fff',
    fontFamily: 'Raleway Light',

  },
  search_btn:{
    position: 'absolute',
    bottom: 100,
    left: 20,
    right: 20,
    backgroundColor: '#295480'
  }
})